package nl.codesoup.blendle;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class FontDescriptor {
    public abstract String family();

    public abstract String style();

    public abstract int weight();

    public static FontDescriptor create(String family, String style, int weight) {
        return new AutoValue_FontDescriptor(family, style, weight);
    }

    public abstract FontDescriptor withWeight(int weight);

    public abstract FontDescriptor withStyle(String style);

    public abstract FontDescriptor withFamily(String family);
}
