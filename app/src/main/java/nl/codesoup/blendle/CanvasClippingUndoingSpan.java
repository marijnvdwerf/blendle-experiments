package nl.codesoup.blendle;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Region;
import android.text.style.LineBackgroundSpan;

public class CanvasClippingUndoingSpan implements LineBackgroundSpan {
    private int padding;
    private Rect bounds;


    public CanvasClippingUndoingSpan(int padding) {
        this.padding = padding;
        bounds = new Rect();
    }

    @Override
    public void drawBackground(Canvas c, Paint p, int left, int right, int top, int baseline, int bottom, CharSequence text, int start, int end, int lnum) {
        if (lnum != 0) {
            return;
        }

        c.getClipBounds(bounds);
        bounds.left -= padding;
        bounds.right += padding;
        c.clipRect(bounds, Region.Op.UNION);
    }
}
