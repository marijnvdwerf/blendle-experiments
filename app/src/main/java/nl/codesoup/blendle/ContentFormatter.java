package nl.codesoup.blendle;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.LeadingMarginSpan;
import android.text.style.MetricAffectingSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.css.sac.Selector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import nl.codesoup.blendle.api.Item;
import nl.codesoup.blendle.api.ProviderConfigurations.ProviderConfiguration;
import nl.codesoup.blendle.api.ProviderStyle;
import nl.marijnvdwerf.blendle.style.HtmlElement;
import nl.marijnvdwerf.blendle.style.SelectorParser;
import nl.marijnvdwerf.blendle.style.StyleDeclaration;
import nl.marijnvdwerf.blendle.style.StyleParser;
import nl.marijnvdwerf.blendle.style.properties.BackgroundColor;
import nl.marijnvdwerf.blendle.style.properties.FontFamily;
import nl.marijnvdwerf.blendle.style.properties.FontSize;
import nl.marijnvdwerf.blendle.style.properties.FontStyleDeclaration;
import nl.marijnvdwerf.blendle.style.properties.FontWeight;
import nl.marijnvdwerf.blendle.style.properties.LetterSpacing;
import nl.marijnvdwerf.blendle.style.properties.LineHeight;
import nl.marijnvdwerf.blendle.style.properties.MarginBottom;
import nl.marijnvdwerf.blendle.style.properties.TextAlign;
import nl.marijnvdwerf.blendle.style.properties.TextDecoration;
import nl.marijnvdwerf.blendle.style.properties.TextIndent;
import nl.marijnvdwerf.blendle.style.properties.TextTransform;
import nl.marijnvdwerf.blendle.style.value.ColorValue;
import nl.marijnvdwerf.blendle.style.value.RGBColorValue;
import timber.log.Timber;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class ContentFormatter {
    private static List<BodyElement> getItemContent(Item item, ProviderConfiguration configuration) {
        List<BodyElement> elements = new ArrayList<>();

        if (configuration == null) {
            elements.addAll(item.manifest().body());
            return elements;
        }

        String[] segments = configuration.templates().get("tile").split("\\s");
        for (String type : segments) {
            type = type.replace("{", "").replace("}", "");

            if (type.equals("content")) {
                for (BodyElement element : item.manifest().body()) {
                    if (element.type().toLowerCase().equals("p") || element.type().toLowerCase().equals("ph")) {
                        elements.add(element);
                    }
                }
            } else {
                elements.addAll(getElements(item, type));
            }
        }

        return elements;
    }

    private static Collection<BodyElement> getElements(Item item, String type) {
        List<BodyElement> elements = new ArrayList<>();

        for (BodyElement element : item.manifest().body()) {
            if (type.equalsIgnoreCase(element.type())) {
                elements.add(element);
            }
        }

        return elements;
    }

    private static int convertColor(ColorValue color) {
        if (color instanceof RGBColorValue) {
            RGBColorValue rgbColor = (RGBColorValue) color;
            return Color.rgb(rgbColor.red(), rgbColor.green(), rgbColor.blue());
        }

        return Color.MAGENTA;
    }

    private static List<StyleDeclaration> getStyles(ProviderStyle providerStyle, HtmlElement... elements) {
        if (providerStyle == null) {
            return new ArrayList<>();
        }

        StyleParser styleParser = new StyleParser();
        styleParser.setLogger(new TimberLogger(Timber.asTree()));

        SelectorParser selectorParser = new SelectorParser();
        selectorParser.setLogger(new TimberLogger(Timber.asTree()));

        List<StyleDeclaration> styleDeclarations = new ArrayList<>();
        for (ProviderStyle.Style style : providerStyle.styles()) {
            List<Selector> selectors = selectorParser.parseSelector(style.selector());

            boolean matches = false;
            for (Selector selector : selectors) {
                if (selectorParser.matchesSelector(selector, elements)) {
                    matches = true;
                    break;
                }
            }

            if (!matches) {
                continue;
            }

            for (Map.Entry<String, String> entry : style.attributes().entrySet()) {
                StyleDeclaration[] declarations = styleParser.parseDeclaration(entry.getKey(), entry.getValue());
                for (StyleDeclaration declaration : declarations) {
                    styleDeclarations.add(declaration);
                }
            }
        }

        return styleDeclarations;
    }

    public ViewGroup formatContent(Context context, Item item, ProviderConfiguration configuration, ProviderStyle stylesheet, String styleName, Map<FontDescriptor, Typeface> fonts) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();

        final int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16f, metrics);

        LinearLayout container = new LinearLayout(context);
        container.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams defaultLayoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        HtmlElement containerElement = HtmlElement.create("div").withId("mobile").withClassName("provider-" + styleName);

        List<BodyElement> bodyElements = getItemContent(item, configuration);
        for (int i = 0; i < bodyElements.size(); i++) {
            BodyElement element = bodyElements.get(i);
            TextView textView = new TextView(container.getContext());
            textView.setPadding(padding, (i == 0 ? padding : 0), padding, 0);
            textView.getPaint().setHinting(Paint.HINTING_OFF);

            textView.setTextLocale(Locale.GERMAN);
            textView.setBreakStrategy(Layout.BREAK_STRATEGY_HIGH_QUALITY);

            SpannableString content = SpannableString.valueOf(String.valueOf(element.content()));
            content.setSpan(new CanvasClippingUndoingSpan(padding), 0, content.length(), 0);

            textView.setTextColor(Color.BLACK);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(defaultLayoutParams);

            HtmlElement blockElement;
            switch (element.type()) {
                case "hl1":
                    blockElement = HtmlElement.create("h1").withClassName("item-title");
                    break;

                case "hl2":
                    blockElement = HtmlElement.create("h2").withClassName("item-subtitle");
                    break;

                case "p":
                    blockElement = HtmlElement.create("p").withClassName("item-paragraph");
                    break;

                case "ph":
                    blockElement = HtmlElement.create("h3").withClassName("item-header");
                    break;

                case "lead":
                case "kicker":
                case "byline":
                case "dateline":
                case "intro":
                default:
                    blockElement = HtmlElement.create("p").withClassName("item-" + element.type());
                    break;
            }

            List<StyleDeclaration>
                    styles = getStyles(stylesheet, blockElement, containerElement);


            String fontFamily = "Comic Sans MS";
            String fontStyle = "normal";
            int fontWeight = 0;
            for (StyleDeclaration style : styles) {

                if (style instanceof FontStyleDeclaration) {
                    switch (((FontStyleDeclaration) style).value()) {
                        case FontStyleDeclaration.FONT_STYLE_ITALIC:
                            fontStyle = "italic";
                            break;
                        case FontStyleDeclaration.FONT_STYLE_NORMAL:
                            fontStyle = "normal";
                            break;
                    }
                }

                if (style instanceof FontWeight) {
                    fontWeight = ((FontWeight) style).value();
                }

                if (style instanceof FontFamily) {
                    fontFamily = ((FontFamily) style).name();
                }
            }

            FontDescriptor descriptor = FontDescriptor.create(fontFamily, fontStyle, fontWeight);
            if (fontFamily != null && fontWeight != 0) {
                if (fonts.containsKey(descriptor)) {
                    Typeface font = fonts.get(descriptor);
                    textView.setTypeface(font);
                } else {
                    Timber.e("Font not found: %s", descriptor);
                }
            } else {
                Timber.e("Not all font properties were set");
            }

            for (StyleDeclaration style : styles) {
                if (style instanceof FontSize) {
                    textView.setTextSize(((FontSize) style).size);
                }
            }

            for (StyleDeclaration style : styles) {
                if (style instanceof nl.marijnvdwerf.blendle.style.properties.Color) {
                    int colorInt = convertColor(((nl.marijnvdwerf.blendle.style.properties.Color) style).color());
                    textView.setTextColor(colorInt);
                }

                if (style instanceof TextTransform) {
                    if (((TextTransform) style).transform == TextTransform.TRANSFORM_UPPERCASE) {
                        SpannableString newContent = SpannableString.valueOf(String.valueOf(content).toUpperCase(Locale.forLanguageTag("nl-NL")));
                        for (Object span : content.getSpans(0, content.length(), Object.class)) {
                            newContent.setSpan(span, content.getSpanStart(span), content.getSpanEnd(span), content.getSpanFlags(span));
                        }
                        content = newContent;
                    }
                }

                if (style instanceof LineHeight) {
                    float lineHeightSp = ((LineHeight) style).lineHeight;
                    float lineHeightPx = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, lineHeightSp, metrics));
                    Paint.FontMetricsInt fontMetrics = textView.getPaint().getFontMetricsInt();
                    int baseLineHeightPx = -fontMetrics.ascent + fontMetrics.descent;
                    int diff = (int) (lineHeightPx - baseLineHeightPx);
                    textView.setLineSpacing(diff, 1);

                    int topOffset = Math.round(diff / 2);
                    int bottomOffset = diff - topOffset;

                    if (topOffset < 0) {
                        layoutParams.topMargin += topOffset;
                        topOffset = 0;
                    }
                    if (bottomOffset < 0) {
                        layoutParams.bottomMargin += bottomOffset;
                        bottomOffset = 0;
                    }
                    textView.setPadding(textView.getPaddingLeft(), textView.getPaddingTop() + topOffset, textView.getPaddingRight(), textView.getPaddingBottom() + bottomOffset);
                }

                if (style instanceof TextIndent) {
                    int indentPx = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, ((TextIndent) style).length, metrics));

                    content.setSpan(new LeadingMarginSpan.Standard(indentPx, 0), 0, content.length(), Spanned.SPAN_PARAGRAPH);
                }

                if (style instanceof TextDecoration) {
                    if (((TextDecoration) style).value() == TextDecoration.TEXT_DECORATION_UNDERLINE) {
                        content.setSpan(new TypographyActivity.BetterUnderlineSpan(textView.getTextSize() / 16f), 0, content.length(), Spanned.SPAN_PARAGRAPH);
                    }
                }


                if (style instanceof BackgroundColor) {
                    int colorInt = convertColor(((BackgroundColor) style).color());
                    content.setSpan(new BackgroundColorSpan(colorInt), 0, content.length(), 0);
                }

                if (style instanceof TextAlign) {
                    switch (((TextAlign) style).value()) {
                        case TextAlign.TEXT_ALIGN_LEFT:
                            textView.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                            break;

                        case TextAlign.TEXT_ALIGN_RIGHT:
                            textView.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
                            break;

                        case TextAlign.TEXT_ALIGN_CENTER:
                            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            break;

                        case TextAlign.TEXT_ALIGN_JUSTIFY:
                            Timber.e("Justified alignment isn't supported on Android");
                            break;
                    }
                }

                if (style instanceof MarginBottom) {
                    layoutParams.bottomMargin = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, ((MarginBottom) style).bottom, metrics));
                }

                if (style instanceof LetterSpacing) {
                    float spacing = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, ((LetterSpacing) style).value(), metrics);
                    textView.setLetterSpacing(spacing / textView.getTextSize());
                }
            }

            Spanned htmlContent = element.content();
            HtmlElement[] spans = htmlContent.getSpans(0, htmlContent.length(), HtmlElement.class);
            for (HtmlElement spanElement : spans) {
                List<StyleDeclaration> inlineStyles = getStyles(stylesheet, spanElement, blockElement, containerElement);
                final int start = htmlContent.getSpanStart(spanElement);
                final int end = htmlContent.getSpanEnd(spanElement);

                FontDescriptor inlineFontDescriptor = descriptor;

                for (StyleDeclaration style : inlineStyles) {
                    if (style instanceof FontWeight) {
                        inlineFontDescriptor = inlineFontDescriptor.withWeight(((FontWeight) style).value());
                    }

                    if (style instanceof FontFamily) {
                        inlineFontDescriptor = inlineFontDescriptor.withFamily(((FontFamily) style).name());
                    }

                    if (style instanceof FontStyleDeclaration) {
                        Timber.e("font-style not implemented for inline-elements");
                    }
                }

                if (!inlineFontDescriptor.equals(descriptor)) {

                    if (fonts.containsKey(inlineFontDescriptor)) {
                        final Typeface inlineFont = fonts.get(inlineFontDescriptor);
                        content.setSpan(new MetricAffectingSpan() {

                            @Override
                            public void updateDrawState(TextPaint paint) {
                                paint.setTypeface(inlineFont);
                            }

                            @Override
                            public void updateMeasureState(TextPaint paint) {
                                paint.setTypeface(inlineFont);
                            }
                        }, start, end, 0);
                    } else {
                        Timber.e("Font not found: %s", descriptor);
                    }
                }

                for (StyleDeclaration style : styles) {
                    if (style instanceof FontSize) {
                        float pixelSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, ((FontSize) style).size, metrics);
                        content.setSpan(new AbsoluteSizeSpan(Math.round(pixelSize)), start, end, 0);
                    }
                }

                for (StyleDeclaration style : inlineStyles) {
                    if (style instanceof nl.marijnvdwerf.blendle.style.properties.Color) {
                        nl.marijnvdwerf.blendle.style.properties.Color colorStyle = (nl.marijnvdwerf.blendle.style.properties.Color) style;
                        content.setSpan(new ForegroundColorSpan(convertColor(colorStyle.color())), start, end, 0);
                    }
                }

                Timber.d("inline styles: %s", inlineStyles);
            }

            textView.setText(content);
            container.addView(textView, layoutParams);
        }

        return container;
    }
}
