package nl.codesoup.blendle;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.text.AllCapsTransformationMethod;
import android.text.Layout;
import android.text.SpannableString;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.style.ForegroundColorSpan;
import android.text.style.LineBackgroundSpan;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.TextView;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class TypographyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Typeface helveticaNeueCondensed = Typeface.createFromAsset(getAssets(), "HelveticaNeue-CondensedBold.ttf");
        Typeface playfairDisplayItalic = Typeface.createFromAsset(getAssets(), "PlayfairDisplay-Italic.ttf");
        Typeface playfairDisplay = Typeface.createFromAsset(getAssets(), "PlayfairDisplay-Regular.ttf");
        Typeface droidSerif = Typeface.createFromAsset(getAssets(), "DroidSerif-Regular.ttf");

        Typeface bodoni72 = Typeface.createFromAsset(getAssets(), "BodoniSvtyTwoITCTT-Bold.ttf");

        BodyElement[] body = new BodyElement[]{
                BodyElement.create("telegraaf-hl1", "Verdwenen of gewist?"),
                BodyElement.create("telegraaf-hl1", "Bizar: hongerige dief bakt burgers"),
                BodyElement.create("hl2", "Na de kernramp"),
                BodyElement.create("kicker", "fotodocument"),
                BodyElement.create("hl1", "Stralend landschap"),
                BodyElement.create("intro",
                        "Duizenden mannen in witte pakken proberen het gebied rond Fukushima te " +
                                "ontdoen van radioactieve straling. Maar de geëvacueerde " +
                                "bewoners keren niet terug."),
                BodyElement.create("byline", "ROBERT KNOTH EN ANTOINETTE DE JONG"),
                BodyElement.create("p",
                        "Al in de herfst na de kernramp van maart 2011 renden de everzwijnen " +
                                "door de straten van Tsushima, een dorp waarvan alle inwoners " +
                                "geëvacueerd werden. Ook andere dieren kregen in de gaten dat " +
                                "hun territorium groter was geworden. Priester Hiroshi Tada zag " +
                                "het jaar erop vanuit zijn huis bij de tempel in \u00ADItate een " +
                                "troep makaken de bergen afkomen. ‘Het waren er wel tweehonderd. " +
                                "Zoveel had ik er nog nooit gezien.’"),
                BodyElement.create("p", "Tijdens onze reizen tussen…"),
        };

        LinearLayout container = (LinearLayout) findViewById(R.id.container);
        LinearLayout.LayoutParams defaultLayoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        final int horizontalMargin = getResources().getDimensionPixelOffset(R.dimen.activity_horizontal_margin);

        int[] colors = new int[]{Color.RED, Color.BLUE};
        int color = -1;
        for (BodyElement element : body) {
            TextView textView = new TextView(this);
            textView.setPadding(horizontalMargin, 0, horizontalMargin, 0);
            textView.getPaint().setHinting(Paint.HINTING_OFF);

            SpannableString content = SpannableString.valueOf(element.content());
            content.setSpan(new CanvasClippingUndoingSpan(horizontalMargin), 0, content.length(), 0);

            textView.setTextColor(Color.BLACK);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(defaultLayoutParams);

            color++;
            if (color == colors.length) {
                color = 0;
            }
            //textView.setBackgroundColor(colors[color]);

            switch (element.type()) {
                case "kicker":
                    textView.setTypeface(helveticaNeueCondensed);
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    setLineHeight(24f, textView);
                    textView.setTransformationMethod(new AllCapsTransformationMethod(this));
                    break;

                case "hl1":
                    textView.setTypeface(helveticaNeueCondensed);
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
                    setLineHeight(36f, textView);
                    layoutParams.bottomMargin = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 20, getResources().getDisplayMetrics()));
                    break;

                case "intro":
                    textView.setTypeface(playfairDisplayItalic);
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
                    setLineHeight(26f, textView);
                    layoutParams.bottomMargin = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 20, getResources().getDisplayMetrics()));
                    break;

                case "byline":
                    textView.setTypeface(playfairDisplay);
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                    setLineHeight(24f, textView);
                    textView.setTransformationMethod(new AllCapsTransformationMethod(this));
                    layoutParams.bottomMargin = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 20, getResources().getDisplayMetrics()));
                    break;

                case "hl2":
                    textView.setTypeface(helveticaNeueCondensed);
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    textView.setTextColor(0xFFE3000B);
                    textView.setLetterSpacing(0.6f / 18f);
                    setLineHeight(24f, textView);
                    textView.setTransformationMethod(new AllCapsTransformationMethod(this));
                    layoutParams.bottomMargin = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 20, getResources().getDisplayMetrics()));
                    break;

                default:
                case "p":
                    textView.setTypeface(droidSerif);
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                    // line height 27
                    setLineHeight(27f, textView);
                    layoutParams.bottomMargin = 0;
                    break;

                case "telegraaf-hl1":
                    textView.setTypeface(bodoni72);
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
                    setLineHeight(36f, textView);
                    content.setSpan(new BetterUnderlineSpan(3f), 0, content.length(), 0);
                    textView.setText(content);
                    layoutParams.bottomMargin = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 20, getResources().getDisplayMetrics()));
                    break;
            }

            textView.setText(content);
            container.addView(textView, layoutParams);
        }
    }

    private void setLineHeight(final float lineHeight, final TextView textView) {
        float lineHeightPx = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, lineHeight, getResources().getDisplayMetrics()));
        Paint.FontMetrics fontMetrics = textView.getPaint().getFontMetrics();
        float diff = lineHeightPx / (-fontMetrics.ascent + fontMetrics.descent);
        textView.setLineSpacing(0, diff);
    }

    public static class BetterUnderlineSpan implements LineBackgroundSpan {

        private final float thickness;

        public BetterUnderlineSpan(float thickness) {
            this.thickness = thickness;
        }

        @Override
        public void drawBackground(Canvas canvas, Paint paint, int left, int right, int top, int baseline, int bottom, CharSequence text, int start, int end, int lnum) {
            //Create a temporary bitmap the size of this line
            Bitmap tempBitmap = Bitmap.createBitmap(right - left, bottom - top, Bitmap.Config.ALPHA_8);
            Canvas tempCanvas = new Canvas(tempBitmap);

            int relative_baseline = baseline - top;

            String subString = String.valueOf(text.subSequence(start, end));
            subString = subString.replaceAll("\\s+$", "");
            float width = paint.measureText(subString, 0, subString.length());

            Paint underlinePaint = new Paint();
            underlinePaint.setColor(Color.YELLOW);
            int underlineOffset = (int) Math.floor(thickness);
            tempCanvas.save();
            tempCanvas.translate(0, relative_baseline + thickness);
            tempCanvas.drawRect(0, 0, width, thickness, underlinePaint);
            tempCanvas.restore();

            TextPaint strokePaint = new TextPaint(paint);
            strokePaint.setStyle(Paint.Style.FILL_AND_STROKE);
            strokePaint.setStrokeWidth(thickness * 2);
            strokePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));

            SpannableString wrappedText = SpannableString.valueOf(text);
            wrappedText.setSpan(new ForegroundColorSpan(Color.TRANSPARENT), start, end, 0);
            for (Object span : wrappedText.getSpans(0, wrappedText.length(), this.getClass())) {
                // Prevent recursion
                wrappedText.removeSpan(span);
            }

            int offset = start;
            boolean overlapsUnderline = false;
            while (offset < end) {
                final int codepoint = Character.codePointAt(text, offset);
                int size = Character.charCount(codepoint);

                Rect bounds = new Rect();
                String text1 = String.valueOf(text);
                strokePaint.getTextBounds(text1, offset, offset + size, bounds);
                if (bounds.bottom > underlineOffset) {
                    overlapsUnderline = true;
                    wrappedText.setSpan(new ForegroundColorSpan(Color.MAGENTA), offset, offset + size, 0);
                }
                offset += size;
            }

            if (overlapsUnderline) {
                Layout layout = new StaticLayout(wrappedText, start, end, strokePaint, (int) width * 2, Layout.Alignment.ALIGN_NORMAL, 1, 0, false);

                tempCanvas.translate(0, relative_baseline + paint.getFontMetricsInt().ascent);
                layout.draw(tempCanvas);
            }

            canvas.drawBitmap(tempBitmap, left, top, paint);
            tempBitmap.recycle();
        }
    }


}
