package nl.codesoup.blendle;

import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;

import nl.codesoup.blendle.api.Blendle;
import nl.codesoup.blendle.api.BlendleBuilder;
import nl.codesoup.blendle.api.Item;
import nl.codesoup.blendle.api.ProviderConfigurations;
import nl.codesoup.blendle.api.ProviderStyle;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    public ProviderConfigurations providerConfigurations;
    private int loadCount;
    private Item item;
    private Blendle blendle;
    private ProviderStyle style;
    private HashMap<FontDescriptor, Typeface> fonts = new HashMap<>();
    private ColorMatrix colorMatrix = ColorUtils.sepia;

    private ProviderConfigurations.ProviderConfiguration getProviderConfiguration(String providerId) {
        for (ProviderConfigurations.ProviderConfiguration configuration : providerConfigurations.configurations()) {
            if (configuration.id().equalsIgnoreCase(providerId)) {
                return configuration;
            }
        }

        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        blendle = BlendleBuilder.build();

        loadCount = 0;

        blendle.getProviderConfigurations().enqueue(new Callback<ProviderConfigurations>() {
            @Override
            public void onResponse(Call<ProviderConfigurations> call, Response<ProviderConfigurations> response) {
                if (response.isSuccessful()) {
                    providerConfigurations = response.body();
                    loadCount++;
                    showContent();
                } else {
                    Timber.e("Error fetching %s", call.request().url());
                }
            }

            @Override
            public void onFailure(Call<ProviderConfigurations> call, Throwable t) {
                Timber.e(t, "Error fetching %s", call.request().url());
            }
        });

        blendle.getItem("bnl-neon-20160411-154231_die_pornogest_ndnisse").enqueue(new Callback<Item>() {
            @Override
            public void onResponse(Call<Item> call, Response<Item> response) {
                if (response.isSuccessful()) {
                    item = response.body();
                    loadStyle(item.manifest().provider().id());
                    loadCount++;
                    showContent();
                } else {
                    Timber.e("Error fetching %s", call.request().url());
                }
            }

            @Override
            public void onFailure(Call<Item> call, Throwable t) {
                Timber.e(t, "Error fetching %s", call.request().url());
            }
        });
    }

    private void loadStyle(String providerId) {
        blendle.getStyle(providerId).enqueue(new Callback<ProviderStyle>() {
            @Override
            public void onResponse(Call<ProviderStyle> call, Response<ProviderStyle> response) {
                if (response.isSuccessful()) {
                    style = response.body();
                    loadFonts(style);
                    loadCount++;
                    showContent();
                } else {
                    Timber.e("Error fetching %s", call.request().url());
                }
            }

            @Override
            public void onFailure(Call<ProviderStyle> call, Throwable t) {
                Timber.e(t, "Error fetching %s", call.request().url());
            }
        });
    }

    private void loadFonts(ProviderStyle style) {
        OkHttpClient client = new OkHttpClient();

        for (final ProviderStyle.FontFace fontFace : style.resources().fontFaces()) {
            Request request = new Request.Builder()
                    .get().url(fontFace.src().toString())
                    .build();

            String name = fontFace.src().getLastPathSegment();

            final FontDescriptor descriptor = FontDescriptor.create(fontFace.fontFamily(), fontFace.fontStyle(), fontFace.fontWeight());
            final File cachepath = new File(getCacheDir().getPath() + File.pathSeparator + "fonts" + File.pathSeparator + name);

            if (cachepath.exists()) {
                fonts.put(descriptor, Typeface.createFromFile(cachepath));
                showContent();
            } else {
                client.newCall(request).enqueue(new okhttp3.Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        cachepath.getParentFile().mkdir();
                        InputStream input = response.body().byteStream();

                        try {
                            OutputStream output = new FileOutputStream(cachepath.getPath());
                            try {
                                try {
                                    byte[] buffer = new byte[4 * 1024]; // or other buffer size
                                    int read;

                                    while ((read = input.read(buffer)) != -1) {
                                        output.write(buffer, 0, read);
                                    }
                                    output.flush();
                                } finally {
                                    output.close();
                                }
                            } catch (Exception e) {
                                e.printStackTrace(); // handle exception, define IOException and others
                            }
                        } finally {
                            input.close();
                        }

                        fonts.put(descriptor, Typeface.createFromFile(cachepath));

                        findViewById(android.R.id.content).getHandler().post(new Runnable() {
                            @Override
                            public void run() {
                                showContent();
                            }
                        });
                    }
                });
            }
        }
    }

    public void showContent() {
        if (loadCount != 3) {
            return;
        }

        if (fonts.size() != style.resources().fontFaces().size()) {
            return;
        }

        HyphenatorPatcher.patchHyphenator(this, "nl");
        HyphenatorPatcher.patchHyphenator(this, "de");

        getProviderConfiguration(item.manifest().provider().id());

        findViewById(R.id.progressBar).setVisibility(View.GONE);

        ViewGroup cardView = (ViewGroup) findViewById(R.id.card);
        GradientDrawable background = new GradientDrawable();
        background.setColor(ColorUtils.applyColorMatrix(Color.WHITE, colorMatrix));
        background.setCornerRadius(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3f, getResources().getDisplayMetrics()));
        cardView.setBackground(background);
    }


}
