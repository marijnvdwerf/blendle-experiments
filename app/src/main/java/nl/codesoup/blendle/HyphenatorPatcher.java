package nl.codesoup.blendle;

import android.content.Context;
import android.text.StaticLayout;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Locale;

import timber.log.Timber;

public class HyphenatorPatcher {
    public static void patchHyphenator(Context context, String language) {
        Class hyphenatorClass = null;
        Class staticLayoutClass = null;
        try {
            staticLayoutClass = StaticLayout.class;
            hyphenatorClass = Class.forName("android.text.Hyphenator");


            Method nLoadHyphenator = staticLayoutClass.getDeclaredMethod("nLoadHyphenator", String.class);
            nLoadHyphenator.setAccessible(true);

            String patternData = IOUtils.toString(context.getAssets().open(language + ".pat.txt"));
            long nativePtr = (long) nLoadHyphenator.invoke(null, patternData);


            Constructor constructor = hyphenatorClass.getDeclaredConstructor(long.class);
            constructor.setAccessible(true);

            Object newInstance = constructor.newInstance(nativePtr);

            Field map = hyphenatorClass.getDeclaredField("sMap");
            map.setAccessible(true);

            HashMap<Locale, Object> localeMap = (HashMap<Locale, Object>) map.get(null);

            Locale dutch = new Locale(language);
            localeMap.put(dutch, newInstance);

        } catch (ReflectiveOperationException | IOException e) {
            Timber.e(e, "Error patching hyphenator");
            if (hyphenatorClass != null) {
                Timber.d(classToString(hyphenatorClass));
            }

            if (staticLayoutClass != null) {
                Timber.d(classToString(staticLayoutClass));
            }
        }
    }

    private static String classToString(Class aClass) {
        String output = "";

        output += aClass.getName() + " {\n";


        Field[] fields = aClass.getDeclaredFields();
        if (fields.length > 0) {
            output += "\n";
            for (Field field : fields) {
                output += "  " + field + "\n";
            }
            output += "\n";
        }

        Constructor[] constructors = aClass.getDeclaredConstructors();
        if (constructors.length > 0) {
            output += "\n";
            for (Constructor constructor : constructors) {
                output += "  " + constructor + "\n";
            }
            output += "\n";
        }

        Method[] methods = aClass.getDeclaredMethods();
        if (constructors.length > 0) {
            output += "\n";
            for (Method method : methods) {
                output += "  " + method + "\n";
            }
            output += "\n";
        }

        output += "}\n";

        return output;
    }
}
