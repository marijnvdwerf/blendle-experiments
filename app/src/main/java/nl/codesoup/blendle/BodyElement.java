package nl.codesoup.blendle;

import android.text.SpannableString;
import android.text.Spanned;

import com.google.auto.value.AutoValue;
import com.google.gson.TypeAdapterFactory;

@AutoValue
public abstract class BodyElement {

    public static TypeAdapterFactory factory() {
        return AutoValue_BodyElement.typeAdapterFactory();
    }

    public abstract String type();

    public abstract Spanned content();

    public static BodyElement create(String type, String content) {
        return new AutoValue_BodyElement(type, SpannableString.valueOf(content));
    }

    public static BodyElement create(String type, Spanned content) {
        return new AutoValue_BodyElement(type, content);
    }
}
