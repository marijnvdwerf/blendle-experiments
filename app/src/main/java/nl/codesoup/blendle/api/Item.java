package nl.codesoup.blendle.api;

import com.google.auto.value.AutoValue;
import com.google.gson.TypeAdapterFactory;

import java.util.List;

import nl.codesoup.blendle.BodyElement;

@AutoValue
public abstract class Item {

    public abstract ItemManifest manifest();

    public static Item create(ItemManifest manifest) {
        return new AutoValue_Item(manifest);
    }

    @AutoValue
    public static abstract class ItemManifest {
        public abstract String id();

        public abstract ItemProvider provider();

        public abstract List<BodyElement> body();

        public static TypeAdapterFactory factory() {
            return AutoValue_Item_ItemManifest.typeAdapterFactory();
        }
    }

    @AutoValue
    public static abstract class ItemProvider {
        public abstract String id();

        public static TypeAdapterFactory factory() {
            return AutoValue_Item_ItemProvider.typeAdapterFactory();
        }
    }
}
