package nl.codesoup.blendle.api;

import com.google.auto.value.AutoValue;

import java.util.List;

@AutoValue
public abstract class Items {

    public abstract List<Item> items();

    public static Items create(List<Item> items) {
        return new AutoValue_Items(items);
    }
}
