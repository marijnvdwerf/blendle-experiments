package nl.codesoup.blendle.api;

import android.net.Uri;
import android.text.Spanned;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Arrays;

import nl.codesoup.blendle.BodyElement;
import nl.codesoup.blendle.SpannedAdapter;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BlendleBuilder {

    public static Blendle build() {
        final Gson gson = new GsonBuilder()
                .setLenient()
                .registerTypeAdapterFactory(ProviderStyle.factory())
                .registerTypeAdapterFactory(ProviderStyle.Resources.factory())
                .registerTypeAdapterFactory(ProviderStyle.FontFace.factory())
                .registerTypeAdapterFactory(ProviderStyle.Style.factory())
                .registerTypeAdapterFactory(Item.ItemManifest.factory())
                .registerTypeAdapterFactory(Item.ItemProvider.factory())
                .registerTypeAdapterFactory(ProviderConfigurations.ProviderConfiguration.factory())
                .registerTypeAdapterFactory(BodyElement.factory())
                .registerTypeAdapterFactory(new AndroidTypeAdapterFactory())
                .registerTypeAdapter(Spanned.class, new SpannedAdapter())
                .registerTypeAdapter(Item.class, new JsonDeserializer<Item>() {
                    @Override
                    public Item deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                        Item.ItemManifest manifest = context.deserialize(json.getAsJsonObject().get("_embedded").getAsJsonObject().get("manifest"), Item.ItemManifest.class);

                        return Item.create(manifest);
                    }
                })
                .registerTypeAdapter(ProviderConfigurations.class, new JsonDeserializer<ProviderConfigurations>() {
                    @Override
                    public ProviderConfigurations deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                        JsonArray array = json.getAsJsonObject().get("_embedded").getAsJsonObject().get("configurations").getAsJsonArray();

                        ProviderConfigurations.ProviderConfiguration[] configurations = new ProviderConfigurations.ProviderConfiguration[array.size()];
                        for (int i = 0; i < array.size(); i++) {
                            configurations[i] = context.deserialize(array.get(i).getAsJsonObject(), ProviderConfigurations.ProviderConfiguration.class);
                        }

                        return ProviderConfigurations.create(Arrays.asList(configurations));
                    }
                })
                .registerTypeAdapter(Items.class, new JsonDeserializer<Items>() {
                    @Override
                    public Items deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                        JsonArray array = json.getAsJsonObject().get("_embedded").getAsJsonObject().get("items").getAsJsonArray();

                        Item[] items = new Item[array.size()];
                        for (int i = 0; i < array.size(); i++) {
                            items[i] = context.deserialize(array.get(i).getAsJsonObject(), Item.class);
                        }

                        return Items.create(Arrays.asList(items));
                    }
                })
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = (new OkHttpClient.Builder()).addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl("https://ws.blendle.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(Blendle.class);
    }

    static class AndroidTypeAdapterFactory implements TypeAdapterFactory {

        @Override
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
            if (type.getRawType().equals(Uri.class)) {
                return (TypeAdapter<T>) new UriTypeAdapter();
            }
            return null;
        }
    }

    static class UriTypeAdapter extends TypeAdapter<Uri> {

        @Override
        public void write(JsonWriter out, Uri value) throws IOException {

        }

        @Override
        public Uri read(JsonReader in) throws IOException {
            return Uri.parse(in.nextString());
        }
    }
}
