package nl.codesoup.blendle.api;

import android.net.Uri;

import com.google.auto.value.AutoValue;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.annotations.SerializedName;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import ch.halarious.core.HalResource;

@AutoValue
public abstract class ProviderStyle implements HalResource {
    public abstract Resources resources();

    public abstract List<Style> styles();

    public static TypeAdapterFactory factory() {
        return new AutoValue_ProviderStyle.ProviderStyleTypeAdapterFactory();
    }

    @AutoValue
    public static abstract class Resources implements HalResource {
        @SerializedName("font-faces")
        public abstract Collection<FontFace> fontFaces();

        public static TypeAdapterFactory factory() {
            return new AutoValue_ProviderStyle_Resources.ResourcesTypeAdapterFactory();
        }
    }

    @AutoValue
    public static abstract class FontFace implements HalResource {
        public abstract String format();

        @SerializedName("font-family")
        public abstract String fontFamily();

        @SerializedName("font-style")
        public abstract String fontStyle();

        @SerializedName("font-weight")
        public abstract int fontWeight();

        public abstract Uri src();

        public static TypeAdapterFactory factory() {
            return new AutoValue_ProviderStyle_FontFace.FontFaceTypeAdapterFactory();
        }
    }

    @AutoValue
    public static abstract class Style implements HalResource {
        public abstract String selector();
        public abstract HashMap<String, String> attributes();

        public static TypeAdapterFactory factory() {
            return new AutoValue_ProviderStyle_Style.StyleTypeAdapterFactory();
        };
    }
}
