package nl.codesoup.blendle.api;

import com.google.auto.value.AutoValue;
import com.google.gson.TypeAdapterFactory;

import java.util.List;
import java.util.Map;

import ch.halarious.core.HalResource;

@AutoValue
public abstract class ProviderConfigurations implements HalResource {
    public abstract List<ProviderConfiguration> configurations();

    public static ProviderConfigurations create(List<ProviderConfiguration> configurations) {
        return new AutoValue_ProviderConfigurations(configurations);
    }

    @AutoValue
    public static abstract class ProviderConfiguration {
        public abstract String id();

        public abstract String name();

        public abstract Map<String, String> templates();

        public static TypeAdapterFactory factory() {
            return AutoValue_ProviderConfigurations_ProviderConfiguration.typeAdapterFactory();
        }
    }

}
