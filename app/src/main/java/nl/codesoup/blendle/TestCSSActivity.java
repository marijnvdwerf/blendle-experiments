package nl.codesoup.blendle;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import java.util.Map;
import java.util.Set;

import nl.codesoup.blendle.api.Blendle;
import nl.codesoup.blendle.api.BlendleBuilder;
import nl.codesoup.blendle.api.ProviderConfigurations;
import nl.codesoup.blendle.api.ProviderStyle;
import nl.marijnvdwerf.blendle.style.StyleParser;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class TestCSSActivity extends AppCompatActivity {

    private Blendle blendle;
    private StyleParser parser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        blendle = BlendleBuilder.build();
        parser = new StyleParser();
        parser.setLogger(new TimberLogger(Timber.asTree()));

        blendle.getProviderConfigurations().enqueue(new Callback<ProviderConfigurations>() {
            @Override
            public void onResponse(Call<ProviderConfigurations> call, Response<ProviderConfigurations> response) {
                if (response.isSuccessful()) {
                    for (ProviderConfigurations.ProviderConfiguration configuration : response.body().configurations()) {
                        loadProviderConfig(configuration.name(), configuration.id());
                    }
                } else {
                    Timber.e("Error fetching %s", call.request().url());
                }
            }

            @Override
            public void onFailure(Call<ProviderConfigurations> call, Throwable t) {
                Timber.e(t, "Error fetching %s", call.request().url());
            }
        });
    }

    private void loadProviderConfig(final String name, final String id) {
        blendle.getStyle(id).enqueue(new Callback<ProviderStyle>() {
            @Override
            public void onResponse(Call<ProviderStyle> call, Response<ProviderStyle> response) {
                if (!response.isSuccessful()) {
                    return;
                }

                Timber.d("# %s (%s)", name, id);
                for (ProviderStyle.Style style : response.body().styles()) {
                    Set<Map.Entry<String, String>> entries = style.attributes().entrySet();
                    for (Map.Entry<String, String> set : entries) {
                        parser.parseDeclaration(set.getKey(), set.getValue());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderStyle> call, Throwable t) {

            }
        });
    }
}
