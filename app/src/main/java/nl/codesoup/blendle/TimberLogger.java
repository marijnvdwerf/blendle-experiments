package nl.codesoup.blendle;

import android.util.Log;

import org.slf4j.Logger;
import org.slf4j.Marker;

import timber.log.Timber;

public class TimberLogger implements Logger {

    private Timber.Tree tree;

    public TimberLogger(Timber.Tree tree) {
        this.tree = tree;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public boolean isTraceEnabled() {
        return false;
    }

    @Override
    public void trace(String msg) {
    }

    @Override
    public void trace(String format, Object arg) {
    }

    @Override
    public void trace(String format, Object arg1, Object arg2) {

    }

    @Override
    public void trace(String format, Object... arguments) {

    }

    @Override
    public void trace(String msg, Throwable t) {

    }

    @Override
    public boolean isTraceEnabled(Marker marker) {
        return false;
    }

    @Override
    public void trace(Marker marker, String msg) {

    }

    @Override
    public void trace(Marker marker, String format, Object arg) {

    }

    @Override
    public void trace(Marker marker, String format, Object arg1, Object arg2) {

    }

    @Override
    public void trace(Marker marker, String format, Object... argArray) {

    }

    @Override
    public void trace(Marker marker, String msg, Throwable t) {

    }

    @Override
    public boolean isDebugEnabled() {
        return true;
    }

    @Override
    public void debug(String msg) {
        tree.d(msg);
    }

    @Override
    public void debug(String format, Object arg) {
        tree.d(format, arg);
    }

    @Override
    public void debug(String format, Object arg1, Object arg2) {
        tree.d(format, arg1, arg2);
    }

    @Override
    public void debug(String format, Object... arguments) {
        tree.d(format, arguments);
    }

    @Override
    public void debug(String msg, Throwable t) {
        tree.d(t, msg);
    }

    @Override
    public boolean isDebugEnabled(Marker marker) {
        return false;
    }

    @Override
    public void debug(Marker marker, String msg) {

    }

    @Override
    public void debug(Marker marker, String format, Object arg) {

    }

    @Override
    public void debug(Marker marker, String format, Object arg1, Object arg2) {

    }

    @Override
    public void debug(Marker marker, String format, Object... arguments) {

    }

    @Override
    public void debug(Marker marker, String msg, Throwable t) {

    }

    @Override
    public boolean isInfoEnabled() {
        return false;
    }

    @Override
    public void info(String msg) {
        tree.log(Log.INFO, msg);
    }

    @Override
    public void info(String format, Object arg) {
        tree.log(Log.INFO, format, arg);
    }

    @Override
    public void info(String format, Object arg1, Object arg2) {
        tree.log(Log.INFO, format, arg1, arg2);
    }

    @Override
    public void info(String format, Object... arguments) {
        tree.log(Log.INFO, format, arguments);
    }

    @Override
    public void info(String msg, Throwable t) {
        tree.log(Log.INFO, t, msg);
    }

    @Override
    public boolean isInfoEnabled(Marker marker) {
        return false;
    }

    @Override
    public void info(Marker marker, String msg) {

    }

    @Override
    public void info(Marker marker, String format, Object arg) {

    }

    @Override
    public void info(Marker marker, String format, Object arg1, Object arg2) {

    }

    @Override
    public void info(Marker marker, String format, Object... arguments) {

    }

    @Override
    public void info(Marker marker, String msg, Throwable t) {

    }

    @Override
    public boolean isWarnEnabled() {
        return false;
    }

    @Override
    public void warn(String msg) {
        tree.log(Log.WARN, msg);
    }

    @Override
    public void warn(String format, Object arg) {
        tree.log(Log.WARN, format, arg);
    }

    @Override
    public void warn(String format, Object... arguments) {
        tree.log(Log.WARN, format, arguments);
    }

    @Override
    public void warn(String format, Object arg1, Object arg2) {
        tree.log(Log.WARN, format, arg1, arg2);
    }

    @Override
    public void warn(String msg, Throwable t) {
        tree.log(Log.WARN, t, msg);
    }

    @Override
    public boolean isWarnEnabled(Marker marker) {
        return false;
    }

    @Override
    public void warn(Marker marker, String msg) {

    }

    @Override
    public void warn(Marker marker, String format, Object arg) {

    }

    @Override
    public void warn(Marker marker, String format, Object arg1, Object arg2) {

    }

    @Override
    public void warn(Marker marker, String format, Object... arguments) {

    }

    @Override
    public void warn(Marker marker, String msg, Throwable t) {

    }

    @Override
    public boolean isErrorEnabled() {
        return true;
    }

    @Override
    public void error(String msg) {
        tree.log(Log.ERROR, msg);
    }

    @Override
    public void error(String format, Object arg) {
        tree.log(Log.ERROR, format, arg);
    }

    @Override
    public void error(String format, Object arg1, Object arg2) {
        tree.log(Log.ERROR, format, arg1, arg2);
    }

    @Override
    public void error(String format, Object... arguments) {
        tree.log(Log.ERROR, format, arguments);
    }

    @Override
    public void error(String msg, Throwable t) {
        tree.log(Log.ERROR, t, msg);
    }

    @Override
    public boolean isErrorEnabled(Marker marker) {
        return false;
    }

    @Override
    public void error(Marker marker, String msg) {

    }

    @Override
    public void error(Marker marker, String format, Object arg) {

    }

    @Override
    public void error(Marker marker, String format, Object arg1, Object arg2) {

    }

    @Override
    public void error(Marker marker, String format, Object... arguments) {

    }

    @Override
    public void error(Marker marker, String msg, Throwable t) {

    }
}
