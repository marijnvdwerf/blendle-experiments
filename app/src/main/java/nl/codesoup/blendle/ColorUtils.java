package nl.codesoup.blendle;

import android.graphics.Color;
import android.graphics.ColorMatrix;

public class ColorUtils {
    public static final ColorMatrix white = new ColorMatrix(new float[]{
            (float) 0.87843137254902, 0, 0, 0, 27,
            0, (float) 0.87843137254902, 0, 0, 27,
            0, 0, (float) 0.87843137254902, 0, 27,
            0, 0, 0, 1, 0
    });

    public static final ColorMatrix sepia = new ColorMatrix(new float[]{
            (float) 0.662745098039216, 0, 0, 0, 79,
            0, (float) 0.749019607843137, 0, 0, 50,
            0, 0, (float) 0.780392156862745, 0, 28,
            0, 0, 0, 1, 0
    });

    public static final ColorMatrix gray = new ColorMatrix(new float[]{
            (float) -0.454901960784314, 0, 0, 0, 209,
            0, (float) -0.454901960784314, 0, 0, 209,
            0, 0, (float) -0.450980392156863, 0, 207,
            0, 0, 0, 1, 0
    });

    public static final ColorMatrix night = new ColorMatrix(new float[]{
            (float) -0.619607843137255, 0, 0, 0, 176,
            0, (float) -0.619607843137255, 0, 0, 176,
            0, 0, (float) -0.619607843137255, 0, 176,
            0, 0, 0, 1, 0
    });

    public static int applyColorMatrix(int color, ColorMatrix colorMatrix) {
        float[] matrix = colorMatrix.getArray();

        float red = Color.red(color);
        float green = Color.green(color);
        float blue = Color.blue(color);
        float alpha = Color.alpha(color);

        float outRed = red * matrix[0] + green * matrix[1] + blue * matrix[2] + alpha * matrix[3] + matrix[4];
        float outGreen = red * matrix[5] + green * matrix[6] + blue * matrix[7] + alpha * matrix[8] + matrix[9];
        float outBlue = red * matrix[10] + green * matrix[11] + blue * matrix[12] + alpha * matrix[13] + matrix[14];
        float outAlpha = red * matrix[15] + green * matrix[16] + blue * matrix[17] + alpha * matrix[18] + matrix[19];

        return Color.argb(Math.round(outAlpha), Math.round(outRed), Math.round(outGreen), Math.round(outBlue));
    }
}
