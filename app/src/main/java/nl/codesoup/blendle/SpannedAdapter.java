package nl.codesoup.blendle;

import android.text.Html;
import android.text.Spanned;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class SpannedAdapter extends TypeAdapter<Spanned> {

    private final BlendleHtmlParser parser;

    public SpannedAdapter() {
        parser = new BlendleHtmlParser();
    }

    @Override
    public void write(JsonWriter out, Spanned value) throws IOException {
        out.jsonValue(Html.toHtml(value));
    }

    @Override
    public Spanned read(JsonReader in) throws IOException {
        return parser.parseHtml(in.nextString());
    }
}
