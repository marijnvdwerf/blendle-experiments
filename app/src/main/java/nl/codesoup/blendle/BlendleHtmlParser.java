package nl.codesoup.blendle;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;

import org.ccil.cowan.tagsoup.HTMLSchema;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import java.io.IOException;
import java.io.StringReader;
import java.util.regex.Pattern;

import nl.marijnvdwerf.blendle.style.HtmlElement;

public class BlendleHtmlParser {
    private final Parser parser;

    public BlendleHtmlParser() {
        parser = new Parser();
        try {
            parser.setProperty(Parser.schemaProperty, new HTMLSchema());
        } catch (org.xml.sax.SAXNotRecognizedException e) {
            // Should not happen.
            throw new RuntimeException(e);
        } catch (org.xml.sax.SAXNotSupportedException e) {
            // Should not happen.
            throw new RuntimeException(e);
        }
    }


    class ContentConverter implements ContentHandler {
        private final String source;
        private final XMLReader reader;

        private final SpannableStringBuilder stringBuilder = new SpannableStringBuilder();

        public ContentConverter(Parser parser, String source) {
            this.reader = parser;
            this.source = source;
        }

        public Spanned convert() {
            reader.setContentHandler(this);

            try {
                reader.parse(new InputSource(new StringReader(source)));
            } catch (IOException | SAXException e) {
                // We are reading from a string. There should not be IO problems.
                throw new RuntimeException(e);
            }

            if (stringBuilder.length() > 0 && Character.isWhitespace(stringBuilder.charAt(stringBuilder.length() - 1))) {
                stringBuilder.delete(stringBuilder.length() - 1, stringBuilder.length());
            }

            return SpannableString.valueOf(stringBuilder);
        }

        @Override
        public void setDocumentLocator(Locator locator) {

        }

        @Override
        public void startDocument() throws SAXException {

        }

        @Override
        public void endDocument() throws SAXException {

        }

        @Override
        public void startPrefixMapping(String prefix, String uri) throws SAXException {

        }

        @Override
        public void endPrefixMapping(String prefix) throws SAXException {

        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
            switch (localName.toLowerCase()) {
                case "html":
                case "body":
                    return;
            }

            HtmlElement element = HtmlElement.create(localName);

            String className = atts.getValue("class");
            if (className != null) {
                element = element.withClassName(className);
            }

            startTag(element);
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            switch (localName.toLowerCase()) {
                case "html":
                case "body":
                    return;
            }

            endTag(HtmlElement.class);
        }

        private void startTag(Object span) {
            int len = stringBuilder.length();
            stringBuilder.setSpan(span, len, len, Spannable.SPAN_MARK_MARK);
        }

        private void endTag(Class kind) {
            SpannableStringBuilder text = stringBuilder;
            int len = text.length();
            Object obj = getLast(text, kind);
            int where = text.getSpanStart(obj);

            if (where != len) {
                text.setSpan(obj, where, len, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else {
                text.removeSpan(obj);
            }
        }

        private Object getLast(Spanned text, Class kind) {
        /*
         * This knows that the last returned object from getSpans()
         * will be the most recently added.
         */
            Object[] objs = text.getSpans(0, text.length(), kind);

            if (objs.length == 0) {
                return null;
            } else {
                for (int i = objs.length - 1; i >= 0; i--) {
                    Object span = objs[i];
                    if (text.getSpanFlags(span) == Spannable.SPAN_MARK_MARK) {
                        return span;
                    }
                }
            }

            return null;
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String characters = String.valueOf(ch, start, length);

            Pattern leadingWhitespace = Pattern.compile("\\A\\p{Space}");
            String input = stringBuilder.toString();

            if (input.length() == 0 || Character.isWhitespace(input.charAt(input.length() - 1))) {
                // remove leading whitespace
                characters = leadingWhitespace.matcher(characters).replaceAll("");
            }

            characters = characters.replaceAll("\\p{Space}+", " ");

            stringBuilder.append(characters);
        }

        @Override
        public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {

        }

        @Override
        public void processingInstruction(String target, String data) throws SAXException {

        }

        @Override
        public void skippedEntity(String name) throws SAXException {

        }
    }

    public Spanned parseHtml(String input) {
        ContentConverter converter = new ContentConverter(parser, input);
        return converter.convert();
    }
}
