package nl.codesoup.robin;

import android.content.Context;
import android.widget.FrameLayout;

public class FixedAspectRatioFrameLayout extends FrameLayout {
    private final float ratio;

    public FixedAspectRatioFrameLayout(Context context, float ratio) {
        super(context);
        this.ratio = ratio;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int originalWidth = MeasureSpec.getSize(widthMeasureSpec);

        super.onMeasure(
                MeasureSpec.makeMeasureSpec(originalWidth, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(Math.round(originalWidth * ratio), MeasureSpec.EXACTLY)
        );
    }
}
