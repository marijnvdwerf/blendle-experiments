package nl.codesoup.robin;

public interface OnAttachmentChangeListener {
    void onAttachmentChange(BlendleAttachmentProcessor.Attachment attachment);
}
