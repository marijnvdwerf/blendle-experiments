package nl.codesoup.robin;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.codesoup.blendle.ContentFormatter;
import nl.codesoup.blendle.FontDescriptor;
import nl.codesoup.blendle.R;
import nl.codesoup.blendle.api.Blendle;
import nl.codesoup.blendle.api.BlendleBuilder;
import nl.codesoup.blendle.api.Item;
import nl.codesoup.blendle.api.ProviderStyle;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class BlendleAttachmentProcessor {
    private final Context context;
    private final Blendle api;
    private final ContentFormatter formatter = new ContentFormatter();
    Map<String, Attachment> attachmentMap = new HashMap<>();
    private Map<String, Call<Item>> itemCalls = new HashMap<>();
    private Map<String, Item> items = new HashMap<>();
    private OkHttpClient fontClient;

    public BlendleAttachmentProcessor(Context context) {
        this.context = context.getApplicationContext();
        api = BlendleBuilder.build();


        int cacheSize = 20 * 1024 * 1024; // 20 MiB
        final File cachepath = new File(context.getCacheDir().getPath() + File.separator + "fonts");
        cachepath.mkdirs();
        Cache cache = new Cache(cachepath, cacheSize);

        fontClient = new OkHttpClient.Builder()
                .cache(cache)
                .build();
    }

    public boolean supportsUrl(HttpUrl url) {
        if (!url.host().equals("blendle.com")) {
            return false;
        }

        if (url.pathSegments().size() < 4) {
            return false;
        }

        if (!url.pathSegments().get(0).equalsIgnoreCase("i")) {
            return false;
        }

        return true;
    }

    private void updateAttachments() {
        for (Attachment attachment : attachmentMap.values()) {
            attachment.update();
        }
    }

    public Attachment getAttachment(HttpUrl url) {
        String id = url.pathSegments().get(3);

        if (attachmentMap.containsKey(id)) {
            return attachmentMap.get(id);
        }

        Attachment attachment = new Attachment(id, this);
        attachmentMap.put(id, attachment);

        return attachment;
    }

    private void loadArticle(final String itemId) {
        if (items.containsKey(itemId)) {
            return;
        }

        if (itemCalls.containsKey(itemId)) {
            return;
        }

        Call<Item> call = api.getItem(itemId);
        itemCalls.put(itemId, call);

        call.enqueue(new Callback<Item>() {
            @Override
            public void onResponse(Call<Item> call, Response<Item> response) {
                if (response.isSuccessful()) {
                    items.put(itemId, response.body());
                    loadProviderStyle(response.body().manifest().provider().id());
                } else {
                    Timber.e("Failed loading item %s: %s", itemId, response.errorBody());
                    items.put(itemId, null);
                }

                itemCalls.remove(itemId);
                updateAttachments();
            }

            @Override
            public void onFailure(Call<Item> call, Throwable t) {
                Timber.e(t, "Failed loading item (%s) from %s", itemId, call.request().url());
                itemCalls.remove(itemId);
                updateAttachments();
            }
        });
    }

    private Map<String, Call<ProviderStyle>> providerStyleCalls = new HashMap<>();
    private Map<String, ProviderStyle> providerStyles = new HashMap<>();

    private void loadProviderStyle(final String providerId) {
        if (providerStyles.containsKey(providerId)) {
            return;
        }

        if (providerStyleCalls.containsKey(providerId)) {
            return;
        }

        Call<ProviderStyle> call = api.getStyle(providerId);
        providerStyleCalls.put(providerId, call);

        call.enqueue(new Callback<ProviderStyle>() {
            @Override
            public void onResponse(Call<ProviderStyle> call, Response<ProviderStyle> response) {
                if (response.isSuccessful()) {
                    providerStyles.put(providerId, response.body());
                } else {
                    Timber.e("Failed loading provider %s: %s", providerId, response.errorBody());
                    providerStyles.put(providerId, null);
                }

                providerStyleCalls.remove(providerId);
                updateAttachments();
            }

            @Override
            public void onFailure(Call<ProviderStyle> call, Throwable t) {
                Timber.e(t, "Failed loading provider %s", providerId);
                providerStyleCalls.remove(providerId);
                providerStyles.put(providerId, null);
                updateAttachments();
            }
        });
    }

    private Map<String, okhttp3.Call> typefaceCalls = new HashMap<>();
    private Map<String, Typeface> typefaces = new HashMap<>();

    private void loadFont(final Uri src) {
        final Request request = new Request.Builder()
                .get().url(src.toString())
                .build();


        if (typefaces.containsKey(src.toString())) {
            return;
        }

        if (typefaceCalls.containsKey(src.toString())) {
            return;
        }

        okhttp3.Call call = fontClient.newCall(request);

        typefaceCalls.put(src.toString(), call);

        call.enqueue(new okhttp3.Callback() {
            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                if (response.isSuccessful()) {
                    File tempFile = File.createTempFile("font", ".ttf", context.getCacheDir());
                    Timber.d("Temp path: %s", tempFile.getAbsolutePath());
                    IOUtils.copy(response.body().byteStream(), new FileOutputStream(tempFile));
                    Typeface typeface = Typeface.createFromFile(tempFile);
                    if (typeface == null) {
                        Timber.d("Error loading typeface: %s", call.request().url());
                    }
                    typefaces.put(src.toString(), typeface);
                    //tempFile.deleteOnExit();
                } else {
                    Timber.e("Failed loading font %s: %s", src, response.message());
                    typefaces.put(src.toString(), null);
                }

                typefaceCalls.remove(src.toString());
                updateAttachments();
            }

            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                Timber.e(e, "Failed loading font %s", src);

                typefaceCalls.remove(src.toString());
                typefaces.put(src.toString(), null);
                updateAttachments();
            }

        });
    }

    public enum State {
        NOT_INITIALIZED,
        LOADING,
        ERROR,
        LOADING_STYLES, LOADING_ARTICLE, LOADING_FONTS, ERROR_LOADING_ARTICLE, ERROR_LOADING_STYLES, ERROR_LOADING_FONTS, DONE
    }

    public class Attachment {
        private final String id;
        private final BlendleAttachmentProcessor parent;
        private State state;

        private FixedAspectRatioFrameLayout container;

        private HashMap<FontDescriptor, Typeface> fonts;
        private OnAttachmentChangeListener listener;

        private Item item;
        private ProviderStyle style;
        private String styleProvider;


        private Attachment(String id, BlendleAttachmentProcessor blendleAttachmentProcessor) {
            this.id = id;
            parent = blendleAttachmentProcessor;
            state = State.NOT_INITIALIZED;
        }

        private void update() {
            if (state == State.DONE) {
                return;
            }

            if (!items.containsKey(id)) {
                if (!itemCalls.containsKey(id)) {
                    loadArticle(id);
                }
                setState(State.LOADING_ARTICLE);
                return;
            }

            Item item = items.get(id);
            if (item == null) {
                // Error loading item
                setState(State.ERROR_LOADING_ARTICLE);
                return;
            }

            this.item = item;

            final String providerId = item.manifest().provider().id();
            if (!providerStyles.containsKey(providerId)) {
                if (!providerStyleCalls.containsKey(providerId)) {
                    loadProviderStyle(providerId);
                }
                setState(State.LOADING_STYLES);
                return;
            }

            ProviderStyle providerStyle = providerStyles.get(providerId);
            if (providerStyle == null) {
                // Try loading default style
                if (!providerStyles.containsKey("default")) {
                    if (!providerStyleCalls.containsKey("default")) {
                        loadProviderStyle("default");
                    }
                    setState(State.LOADING_STYLES);
                    return;
                } else if (providerStyles.containsKey("default") && providerStyles.get("default") != null) {
                    this.styleProvider = "default";
                    this.style = providerStyles.get("default");
                } else {
                    setState(State.ERROR_LOADING_STYLES);
                    return;
                }
            } else {
                this.styleProvider = providerId;
                this.style = providerStyle;
            }

            List<ProviderStyle.FontFace> fontsToLoad = new ArrayList<>();
            for (ProviderStyle.FontFace fontFace : style.resources().fontFaces()) {
                if (!typefaces.containsKey(fontFace.src().toString())) {
                    fontsToLoad.add(fontFace);
                } else if (typefaces.get(fontFace.src().toString()) == null) {
                    setState(State.ERROR_LOADING_FONTS);
                    return;
                }
            }

            if (fontsToLoad.size() > 0) {
                for (ProviderStyle.FontFace font : fontsToLoad) {
                    loadFont(font.src());
                }
                setState(State.LOADING_FONTS);
                return;
            }

            fonts = new HashMap<>();
            for (ProviderStyle.FontFace fontFace : style.resources().fontFaces()) {
                final FontDescriptor descriptor = FontDescriptor.create(fontFace.fontFamily(), fontFace.fontStyle(), fontFace.fontWeight());
                fonts.put(descriptor, typefaces.get(fontFace.src().toString()));
            }

            setState(State.DONE);
        }

        private void setState(State newState) {
            if (newState == state) {
                return;
            }

            state = newState;

            if (listener != null) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onAttachmentChange(Attachment.this);
                    }
                });
            }
        }

        public void setOnChangedListener(OnAttachmentChangeListener listener) {
            this.listener = listener;
        }

        private void renderState() {
            container.removeAllViews();

            Context context = container.getContext();

            switch (state) {
                case NOT_INITIALIZED:
                    container.setBackgroundColor(context.getResources().getColor(R.color.blendle_weed_green));
                    break;

                case ERROR_LOADING_ARTICLE:
                case ERROR_LOADING_STYLES:
                    container.setBackgroundColor(Color.RED);
                    break;

                case LOADING:
                    container.setBackgroundColor(context.getResources().getColor(R.color.blendle_champaign_yellow));
                    break;

                case DONE:
                    container.setBackgroundColor(context.getResources().getColor(R.color.blendle_hot_coral));
                    break;
            }


            TextView textView = new TextView(context);
            switch (state) {
                case LOADING_ARTICLE:
                    textView.setText("Artikel laden...");
                    break;
                case ERROR_LOADING_ARTICLE:
                    textView.setText("Fout bij het laden van het artikel");
                    break;
                case LOADING_STYLES:
                    textView.setText("Vormgeving laden...");
                    break;
                case ERROR_LOADING_STYLES:
                    textView.setText("Fout bij het laden van de vormgeving");
                    break;
                case LOADING_FONTS:
                    textView.setText("Lettertypes laden...");
                    break;
            }

            container.addView(textView, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER));

        }

        public View getView(Context context) {
            if (container != null) {
                //    return container;
            }

            container = new FixedAspectRatioFrameLayout(context, 250f / 306f);

            if (state == State.DONE) {
                ViewGroup viewGroup = formatter.formatContent(context, item, null, style, styleProvider, fonts);

                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.bottomMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, -56, context.getResources().getDisplayMetrics());
                container.addView(viewGroup, layoutParams);
                return container;
            }

            renderState();
            return container;
        }

        public void enqueue() {
            parent.loadArticle(id);
        }
    }

}
