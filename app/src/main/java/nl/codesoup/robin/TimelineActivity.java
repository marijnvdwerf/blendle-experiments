package nl.codesoup.robin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import nl.codesoup.blendle.R;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;


public class TimelineActivity extends AppCompatActivity implements OnAttachmentChangeListener {

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @BindView(R.id.list)
    RecyclerView recyclerView;

    private List<Status> statuses;

    private HashMap<BlendleAttachmentProcessor.Attachment, List<Integer>> attachmentMap = new HashMap<>();
    private BlendleAttachmentProcessor blendle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        blendle = new BlendleAttachmentProcessor(this);

        setContentView(R.layout.activity_twitter);
        ButterKnife.bind(this);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = (new OkHttpClient.Builder()).addInterceptor(interceptor).build();

        Gson gson = (new GsonBuilder())
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setDateFormat("EEE MMM dd HH:mm:ss z yyyy")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://robin.codesoup.nl/demo/")
                .build();

        TwitterApi twitterApi = retrofit.create(TwitterApi.class);

        String query = "from:blendlenl -filter:replies";

        twitterApi.searchTweets(query, SearchResultType.Recent, 40, null, null)
                .enqueue(new Callback<TweetSearchResult>() {
                    @Override
                    public void onResponse(Call<TweetSearchResult> call, Response<TweetSearchResult> response) {
                        if (response.isSuccessful()) {
                            onTweetsLoaded(response.body().statuses);
                        } else {
                            onTweetsFail();
                        }
                    }

                    @Override
                    public void onFailure(Call<TweetSearchResult> call, Throwable t) {

                    }
                });
    }

    private void onTweetsLoaded(List<Status> statuses) {
        this.statuses = statuses;
        progressBar.setVisibility(View.GONE);

        TweetAdapter adapter = new TweetAdapter();

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void onTweetsFail() {

    }

    @Override
    public void onAttachmentChange(BlendleAttachmentProcessor.Attachment attachment) {
        List<Integer> ids = attachmentMap.get(attachment);

        for (Integer id : ids) {
            recyclerView.getAdapter().notifyItemChanged(id);
        }
    }

    class TweetViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text)
        TextView text;

        @BindView(R.id.card)
        CardView card;

        public TweetViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class TweetAdapter extends RecyclerView.Adapter<TweetViewHolder> {

        @Override
        public TweetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new TweetViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.tile_tweet, parent, false));
        }

        @Override
        public void onBindViewHolder(TweetViewHolder holder, int position) {
            Status tweet = statuses.get(position);
            holder.text.setText(tweet.text);

            holder.card.setVisibility(View.GONE);
            holder.card.removeAllViews();
            List<Entity> urls = tweet.entities.get("urls");
            if (urls.size() > 0) {
                Entity lastUrl = urls.get(urls.size() - 1);
                HttpUrl url = HttpUrl.parse(lastUrl.expandedUrl);

                if (blendle.supportsUrl(url)) {
                    BlendleAttachmentProcessor.Attachment attachment = blendle.getAttachment(url);

                    attachment.enqueue();
                    saveAttachment(position, attachment);

                    holder.card.setVisibility(View.VISIBLE);
                    holder.card.addView(attachment.getView(holder.card.getContext()));
                }
            }
        }

        @Override
        public long getItemId(int position) {
            return statuses.get(position).id;
        }

        @Override
        public int getItemCount() {
            return statuses.size();
        }
    }

    private void saveAttachment(int position, BlendleAttachmentProcessor.Attachment attachment) {
        if (attachmentMap.containsKey(attachment)) {
            List<Integer> ids = attachmentMap.get(attachment);
            ids.add(position);
            return;
        }

        ArrayList<Integer> ids = new ArrayList<>();
        ids.add(position);

        attachmentMap.put(attachment, ids);
        attachment.setOnChangedListener(this);
    }

    enum SearchResultType {
        Mixed,
        Recent,
        Popular;

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }

    interface TwitterApi {
        @GET("search/tweets.json")
        public Call<TweetSearchResult> searchTweets(
                @Query("q") String query,
                @Query("result_type") SearchResultType resultType,
                @Query("count") int count,
                @Query("since_id") Long sinceId,
                @Query("max_id") Long maxId
        );
    }

    static class TweetSearchResult {
        List<Status> statuses;
    }

    static class Status {
        protected Date createdAt;
        long id;
        String text;
        Map<String, List<Entity>> entities;
    }

    static class Entity {
        String url;
        String expandedUrl;
        String displayUrl;
        int[] indices;
    }
}
