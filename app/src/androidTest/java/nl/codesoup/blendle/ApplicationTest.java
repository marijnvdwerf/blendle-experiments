package nl.codesoup.blendle;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;

import junit.framework.Assert;

import org.assertj.core.api.Assertions;

import java.util.Arrays;
import java.util.List;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }


    public void testConvertsHTML() {
        BlendleHtmlParser parser = new BlendleHtmlParser();

        SpannableStringBuilder expected = SpannableStringBuilder.valueOf("Laat ");
        // expected.append("de burger ", new BlendleHtmlParser.Em(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        expected.append("er nou ook eens over meepraten");

        Spanned actual = parser.parseHtml("Laat <em> de burger </em> er nou ook eens over meepraten");
        Assert.assertEquals(expected, actual);
    }

    public void testConvertsHTML2() {
        BlendleHtmlParser parser = new BlendleHtmlParser();

        SpannableStringBuilder expected = SpannableStringBuilder.valueOf("Door onze redacteur ");
        //expected.append("Lineke Nieber", new BlendleHtmlParser.Span("person"), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        Spanned actual = parser.parseHtml("Door onze redacteur <span class=\"person\"> Lineke Nieber </span>");
        //Log.d("MJ", actual.getSpans(0, actual.length(), BlendleHtmlParser.Span.class)[0].toString());
        // Log.d("MJ", expected.getSpans(0, expected.length(), BlendleHtmlParser.Span.class)[0].toString());
        Assert.assertEquals(expected, actual);
    }

    public void testNestedSpans() {
        BlendleHtmlParser parser = new BlendleHtmlParser();

        SpannableStringBuilder expected = SpannableStringBuilder.valueOf("");
        // expected.append("Klokkenluiders ", new BlendleHtmlParser.Span("keyword"), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        expected.append("Waar ga je naartoe als je over miljoenen gevoelige documenten beschikt die je wil lekken?");
        //expected.setSpan(new BlendleHtmlParser.Span("intro"), 0, expected.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        Spanned actual = parser.parseHtml("<span class=\"intro\"> <span class=\"keyword\"> Klokkenluiders </span> Waar ga je naartoe als je over miljoenen gevoelige documenten beschikt die je wil lekken? </span>");

        List<Object> expectedSpans = Arrays.asList(expected.getSpans(0, expected.length(), Object.class));
        List<Object> actualSpans = Arrays.asList(actual.getSpans(0, actual.length(), Object.class));
        Assertions.assertThat(actualSpans).hasSameElementsAs(expectedSpans);
        Assertions.assertThat(Html.toHtml(actual)).isEqualTo(Html.toHtml(expected));
    }
}