package nl.marijnvdwerf.blendle.style;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.impl.SimpleLogger;
import org.slf4j.impl.SimpleLoggerFactory;
import org.w3c.css.sac.Selector;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SelectorParserTest {

    private SelectorParser parser;

    @Before
    public void setup() {
        parser = new SelectorParser();

        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
        SimpleLoggerFactory factory = new SimpleLoggerFactory();
        SimpleLogger logger = (SimpleLogger) factory.getLogger(this.getClass().getSimpleName());
        parser.setLogger(logger);
    }

    @Test
    public void testUniversalSelector() {
        Selector selector = getSelector("*");

        HtmlElement element = HtmlElement.create("h2");
        assertThat(parser.matchesSelector(selector, element)).isTrue();
    }

    @Test
    public void testTypeSelector() {
        Selector selector = getSelector("h2");

        HtmlElement element = HtmlElement.create("h2");
        assertThat(parser.matchesSelector(selector, element)).isTrue();

        element = HtmlElement.create("H2");
        assertThat(parser.matchesSelector(selector, element)).isTrue();

        element = HtmlElement.create("p");
        assertThat(parser.matchesSelector(selector, element)).isFalse();
    }

    @Test
    public void testClassSelector() {
        Selector selector = getSelector(".item-title");

        HtmlElement element = HtmlElement.create("h2");
        assertThat(parser.matchesSelector(selector, element)).isFalse();

        element = HtmlElement.create("h2").withClassName("item-title");
        assertThat(parser.matchesSelector(selector, element)).isTrue();

        element = element.withClassName("item-title and a bunch of other classes");
        assertThat(parser.matchesSelector(selector, element)).isTrue();

        element = element.withClassName("Item-title is the wrong case");
        assertThat(parser.matchesSelector(selector, element)).isFalse();

        element = element.withClassName("wrong-class");
        assertThat(parser.matchesSelector(selector, element)).isFalse();
    }

    @Test
    public void testIdSelector() {
        Selector selector = getSelector("#mobile");

        HtmlElement element = HtmlElement.create("div");
        assertThat(parser.matchesSelector(selector, element)).isFalse();

        element = element.withId("❤");
        assertThat(parser.matchesSelector(selector, element)).isFalse();

        element = element.withId("mobile");
        assertThat(parser.matchesSelector(selector, element)).isTrue();

        element = element.withId("Mobile");
        assertThat(parser.matchesSelector(selector, element)).isFalse();
    }

    @Test
    public void testClassAndTypeSelector() {
        Selector selector = getSelector("em.strong");

        HtmlElement element = HtmlElement.create("em");
        assertThat(parser.matchesSelector(selector, element)).isFalse();

        element = HtmlElement.create("div").withClassName("strong");
        assertThat(parser.matchesSelector(selector, element)).isFalse();

        element = HtmlElement.create("em").withClassName("strong");
        assertThat(parser.matchesSelector(selector, element)).isTrue();
    }

    @Test
    public void testClassAndIdSelector() {
        Selector selector = getSelector(".provider-nn#mobile");
        HtmlElement element;

        element = HtmlElement.create("div");
        assertThat(parser.matchesSelector(selector, element)).isFalse();

        element = HtmlElement.create("div").withClassName("provider-nn");
        assertThat(parser.matchesSelector(selector, element)).isFalse();

        element = HtmlElement.create("div").withId("mobile");
        assertThat(parser.matchesSelector(selector, element)).isFalse();

        element = HtmlElement.create("div").withId("mobile").withClassName("provider-nn");
        assertThat(parser.matchesSelector(selector, element)).isTrue();

        element = HtmlElement.create("div").withId("desktop").withClassName("provider-nn");
        assertThat(parser.matchesSelector(selector, element)).isFalse();
    }

    @Test
    public void testDescendantSelector() {
        Selector selector = getSelector(".provider-nn#mobile .item-kicker");
        HtmlElement div = HtmlElement.create("div");
        HtmlElement container = HtmlElement.create("div").withClassName("provider-nn").withId("mobile");
        HtmlElement block = HtmlElement.create("p").withClassName("item-kicker");

        assertThat(parser.matchesSelector(selector, block)).isFalse();
        assertThat(parser.matchesSelector(selector, container)).isFalse();
        assertThat(parser.matchesSelector(selector, block, container)).isTrue();
        assertThat(parser.matchesSelector(selector, block, div, container)).isTrue();
        assertThat(parser.matchesSelector(selector, block, container, div)).isTrue();
        assertThat(parser.matchesSelector(selector, block, div)).isFalse();
        assertThat(parser.matchesSelector(selector, div, container)).isFalse();
    }

    @Test
    public void testNestedDescendantSelector() {
        Selector selector = getSelector(".provider-nn#mobile .item-lead .keyword");
        HtmlElement container = HtmlElement.create("div").withClassName("provider-nn").withId("mobile");
        HtmlElement block = HtmlElement.create("p").withClassName("item-lead");
        HtmlElement span = HtmlElement.create("span");
        HtmlElement keyword = HtmlElement.create("span").withClassName("keyword");

        assertThat(parser.matchesSelector(selector, keyword, block)).isFalse();
        assertThat(parser.matchesSelector(selector, keyword, container)).isFalse();
        assertThat(parser.matchesSelector(selector, keyword, block, container)).isTrue();
        assertThat(parser.matchesSelector(selector, keyword, span, block, container)).isTrue();
        assertThat(parser.matchesSelector(selector, span, keyword, block, container)).isFalse();
    }

    private Selector getSelector(String selector) {
        List<Selector> selectorList = parser.parseSelector(selector);

        assertThat(selectorList).hasSize(1);

        assertThat(selectorList.get(0).toString()).isEqualTo(selector);
        return selectorList.get(0);
    }
}
