package nl.marijnvdwerf.blendle.style;

import org.junit.Before;
import org.junit.Test;

import nl.marijnvdwerf.blendle.style.properties.BackgroundColor;
import nl.marijnvdwerf.blendle.style.properties.Color;
import nl.marijnvdwerf.blendle.style.properties.LetterSpacing;
import nl.marijnvdwerf.blendle.style.properties.TextAlign;
import nl.marijnvdwerf.blendle.style.properties.TextDecoration;
import nl.marijnvdwerf.blendle.style.value.ColorValue;

import static org.assertj.core.api.Assertions.assertThat;

public class StyleParserTest {

    private StyleParser parser;

    @Before
    public void setup() {
        parser = new StyleParser();
    }

    @Test
    public void testTextDecoration() {
        StyleDeclaration[] declarations = parser.parseDeclaration("text-decoration", "underline");
        assertThat(declarations).hasSize(1);
        assertThat(declarations[0]).isEqualTo(TextDecoration.create(TextDecoration.TEXT_DECORATION_UNDERLINE));
    }

    @Test
    public void testTextAlign() {
        StyleDeclaration[] declarations;
        declarations = parser.parseDeclaration("text-align", "left");
        assertThat(declarations).hasSize(1);
        assertThat(declarations[0]).isEqualTo(TextAlign.create(TextAlign.TEXT_ALIGN_LEFT));

        declarations = parser.parseDeclaration("text-align", "right");
        assertThat(declarations).hasSize(1);
        assertThat(declarations[0]).isEqualTo(TextAlign.create(TextAlign.TEXT_ALIGN_RIGHT));

        declarations = parser.parseDeclaration("text-align", "center");
        assertThat(declarations).hasSize(1);
        assertThat(declarations[0]).isEqualTo(TextAlign.create(TextAlign.TEXT_ALIGN_CENTER));

        declarations = parser.parseDeclaration("text-align", "justify");
        assertThat(declarations).hasSize(1);
        assertThat(declarations[0]).isEqualTo(TextAlign.create(TextAlign.TEXT_ALIGN_JUSTIFY));

        declarations = parser.parseDeclaration("text-align", "justify   ");
        assertThat(declarations).hasSize(1);
        assertThat(declarations[0]).isEqualTo(TextAlign.create(TextAlign.TEXT_ALIGN_JUSTIFY));

        declarations = parser.parseDeclaration("text-align", "12");
        assertThat(declarations).isEmpty();

        declarations = parser.parseDeclaration("text-align", "left justify");
        assertThat(declarations).isEmpty();
    }

    @Test
    public void testColor() {
        StyleDeclaration[] declarations;
        declarations = parser.parseDeclaration("color", "rgb(0, 0, 0)");
        assertThat(declarations).hasSize(1);
        assertThat(declarations[0]).isEqualTo(Color.create(ColorValue.rgb(0, 0, 0)));

        declarations = parser.parseDeclaration("color", "rgb(0, 100, 200)");
        assertThat(declarations).hasSize(1);
        assertThat(declarations[0]).isEqualTo(Color.create(ColorValue.rgb(0, 100, 200)));

        declarations = parser.parseDeclaration("color", "#000000");
        assertThat(declarations).hasSize(1);
        assertThat(declarations[0]).isEqualTo(Color.create(ColorValue.rgb(0, 0, 0)));

        declarations = parser.parseDeclaration("color", "#FFFFFF");
        assertThat(declarations).hasSize(1);
        assertThat(declarations[0]).isEqualTo(Color.create(ColorValue.rgb(255, 255, 255)));

        declarations = parser.parseDeclaration("color", "#FFF");
        assertThat(declarations).hasSize(1);
        assertThat(declarations[0]).isEqualTo(Color.create(ColorValue.rgb(255, 255, 255)));

        declarations = parser.parseDeclaration("color", "#000");
        assertThat(declarations).hasSize(1);
        assertThat(declarations[0]).isEqualTo(Color.create(ColorValue.rgb(0, 0, 0)));

        declarations = parser.parseDeclaration("color", "#00000");
        assertThat(declarations).hasSize(0);
    }

    @Test
    public void BackgroundColor() {
        StyleDeclaration[] declarations;
        declarations = parser.parseDeclaration("background-color", "rgb(227, 6, 19)");
        assertThat(declarations).hasSize(1);
        assertThat(declarations[0]).isEqualTo(BackgroundColor.create(ColorValue.rgb(227, 6, 19)));
    }

    @Test
    public void testLetterSpacing() {
        StyleDeclaration[] declarations;
        declarations = parser.parseDeclaration("letter-spacing", "12px");
        assertThat(declarations).hasSize(1);
        assertThat(declarations[0]).isEqualTo(LetterSpacing.create(12));
    }

}
