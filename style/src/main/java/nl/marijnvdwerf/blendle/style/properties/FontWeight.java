package nl.marijnvdwerf.blendle.style.properties;

import nl.marijnvdwerf.blendle.style.StyleDeclaration;

public class FontWeight extends StyleDeclaration {
    private final int weight;

    public FontWeight(int fontWeight) {
        this.weight = fontWeight;
    }

    public int value() {
        return weight;
    }
}
