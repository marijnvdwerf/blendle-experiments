package nl.marijnvdwerf.blendle.style.value;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class RGBColorValue extends ColorValue {

    abstract public int red();

    abstract public int green();

    abstract public int blue();
}
