package nl.marijnvdwerf.blendle.style.properties;

import nl.marijnvdwerf.blendle.style.StyleDeclaration;

public class TextTransform extends StyleDeclaration {
    public final int transform;

    public final static int TRANSFORM_UPPERCASE = 1;

    public TextTransform(int transform) {
        this.transform = transform;
    }
}
