package nl.marijnvdwerf.blendle.style.properties;

import nl.marijnvdwerf.blendle.style.StyleDeclaration;

public class FontFamily extends StyleDeclaration {
    private final String name;

    public FontFamily(String fontName) {
        this.name = fontName;
    }

    public String name() {
        return name;
    }
}
