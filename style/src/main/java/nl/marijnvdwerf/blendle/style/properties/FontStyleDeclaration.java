package nl.marijnvdwerf.blendle.style.properties;

import nl.marijnvdwerf.blendle.style.StyleDeclaration;

public class FontStyleDeclaration extends StyleDeclaration {
    public static final int FONT_STYLE_NORMAL = 0;
    public static final int FONT_STYLE_ITALIC = 1;

    private final int value;

    public FontStyleDeclaration(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}
