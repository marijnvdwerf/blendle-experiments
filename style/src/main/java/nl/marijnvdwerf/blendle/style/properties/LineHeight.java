package nl.marijnvdwerf.blendle.style.properties;

import nl.marijnvdwerf.blendle.style.StyleDeclaration;

public class LineHeight extends StyleDeclaration {
    public final float lineHeight;

    public LineHeight(float lineHeight) {
        this.lineHeight = lineHeight;
    }
}
