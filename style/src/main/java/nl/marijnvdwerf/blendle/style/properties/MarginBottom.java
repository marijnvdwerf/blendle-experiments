package nl.marijnvdwerf.blendle.style.properties;

import nl.marijnvdwerf.blendle.style.StyleDeclaration;

public class MarginBottom extends StyleDeclaration {
    public final float bottom;

    public MarginBottom(float bottom) {
        this.bottom = bottom;
    }
}
