package nl.marijnvdwerf.blendle.style.properties;

import nl.marijnvdwerf.blendle.style.StyleDeclaration;

class Font extends StyleDeclaration {
    private final String name;
    private final int weight;

    public Font(String fontName, int fontWeight) {
        this.name = fontName;
        this.weight = fontWeight;
    }

    public String name() {
        return name;
    }

    public int weight() {
        return weight;
    }
}
