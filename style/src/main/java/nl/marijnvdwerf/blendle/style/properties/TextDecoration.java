package nl.marijnvdwerf.blendle.style.properties;

import com.google.auto.value.AutoValue;

import nl.marijnvdwerf.blendle.style.StyleDeclaration;

@AutoValue
public abstract class TextDecoration extends StyleDeclaration {
    public final static int TEXT_DECORATION_NONE = 0;
    public final static int TEXT_DECORATION_UNDERLINE = 1;

    public abstract int value();

    public static TextDecoration create(int value) {
        return new AutoValue_TextDecoration(value);
    }

}
