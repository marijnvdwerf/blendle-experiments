package nl.marijnvdwerf.blendle.style;

import com.steadystate.css.parser.SACParserCSS3;

import org.slf4j.Logger;
import org.slf4j.helpers.NOPLogger;
import org.w3c.css.sac.AttributeCondition;
import org.w3c.css.sac.CombinatorCondition;
import org.w3c.css.sac.Condition;
import org.w3c.css.sac.ConditionalSelector;
import org.w3c.css.sac.DescendantSelector;
import org.w3c.css.sac.ElementSelector;
import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.Selector;
import org.w3c.css.sac.SelectorList;
import org.w3c.css.sac.SimpleSelector;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectorParser {

    private final SACParserCSS3 parser;
    private Logger logger = NOPLogger.NOP_LOGGER;

    public void setLogger(Logger logger) {
        if (logger != null) {
            this.logger = logger;
        } else {
            this.logger = NOPLogger.NOP_LOGGER;
        }
    }

    public SelectorParser() {
        parser = new SACParserCSS3();
    }

    public List<Selector> parseSelector(String selector) {
        List<Selector> list = new ArrayList<>();

        try {
            SelectorList selectorList = parser.parseSelectors(new InputSource(new StringReader(selector)));
            if (selectorList.getLength() == 1) {
                list.add(selectorList.item(0));
            } else if (selectorList.getLength() > 1) {
                list = new ArrayList<>(selectorList.getLength());
                for (int i = 0; i < selectorList.getLength(); i++) {
                    list.add(selectorList.item(i));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }

    private boolean matchesSimpleSelector(SimpleSelector selector, HtmlElement element) {
        if (selector instanceof ElementSelector) {
            ElementSelector elementSelector = (ElementSelector) selector;

            if (elementSelector.getLocalName() == null) {
                return true;
            }

            return elementSelector.getLocalName().equalsIgnoreCase(element.tagName());

        }

        if (selector instanceof ConditionalSelector) {
            Condition condition = ((ConditionalSelector) selector).getCondition();
            SimpleSelector simpleSelector = ((ConditionalSelector) selector).getSimpleSelector();

            if (!matchesSimpleSelector(simpleSelector, element)) {
                return false;
            }

            return matchesCondition(condition, element);

        }

        logger.error("Selector type ({}) not implemented. ({})", selector.getClass().getSimpleName(), selector);
        return false;
    }

    private boolean matchesCondition(Condition condition, HtmlElement element) {
        if (condition.getConditionType() == Condition.SAC_CLASS_CONDITION) {
            AttributeCondition attributeCondition = (AttributeCondition) condition;
            String className = attributeCondition.getValue();

            for (String elementClass : element.classList()) {
                if (className.contentEquals(elementClass)) {
                    return true;
                }
            }

            return false;
        }

        if (condition.getConditionType() == Condition.SAC_ID_CONDITION) {
            AttributeCondition attributeCondition = (AttributeCondition) condition;
            String id = attributeCondition.getValue();

            return element.id().equals(id);
        }

        if (condition.getConditionType() == Condition.SAC_AND_CONDITION) {
            CombinatorCondition combinatorCondition = (CombinatorCondition) condition;
            return matchesCondition(combinatorCondition.getFirstCondition(), element) && matchesCondition(combinatorCondition.getSecondCondition(), element);
        }

        logger.error("Condition type ({}:{}) not implemented.", condition.getClass().getSimpleName(), condition.getConditionType());
        return false;
    }

    public boolean matchesSelector(Selector selector, HtmlElement... elements) {

        if (selector instanceof SimpleSelector) {
            return matchesSimpleSelector((SimpleSelector) selector, elements[0]);
        }

        if (selector.getSelectorType() == Selector.SAC_DESCENDANT_SELECTOR) {
            DescendantSelector descendantSelector = (DescendantSelector) selector;

            if (!matchesSimpleSelector(descendantSelector.getSimpleSelector(), elements[0])) {
                return false;
            }

            for (int i = 1; i < elements.length; i++) {
                if (matchesSelector(descendantSelector.getAncestorSelector(), Arrays.copyOfRange(elements, i, elements.length))) {
                    return true;
                }
            }

            return false;
        }

        logger.error("Selector type ({}:{}) not implemented. ({})", selector.getClass().getSimpleName(), selector.getSelectorType(), selector);
        return false;
    }
}
