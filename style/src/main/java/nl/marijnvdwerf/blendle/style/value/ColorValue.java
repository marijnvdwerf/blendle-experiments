package nl.marijnvdwerf.blendle.style.value;

import nl.marijnvdwerf.blendle.style.MathUtils;

public abstract class ColorValue {

    public static RGBColorValue rgb(int red, int green, int blue) {
        return new AutoValue_RGBColorValue(
                MathUtils.clamp(red, 0, 255),
                MathUtils.clamp(green, 0, 255),
                MathUtils.clamp(blue, 0, 255)
        );
    }
}
