package nl.marijnvdwerf.blendle.style.properties;

import com.google.auto.value.AutoValue;

import nl.marijnvdwerf.blendle.style.StyleDeclaration;

@AutoValue
public abstract class LetterSpacing extends StyleDeclaration {

    public abstract float value();

    public static LetterSpacing create(float value) {
        return new AutoValue_LetterSpacing(value);
    }
}
