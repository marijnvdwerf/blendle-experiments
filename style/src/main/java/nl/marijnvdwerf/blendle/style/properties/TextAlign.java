package nl.marijnvdwerf.blendle.style.properties;

import com.google.auto.value.AutoValue;

import nl.marijnvdwerf.blendle.style.StyleDeclaration;

@AutoValue
public abstract class TextAlign extends StyleDeclaration {
    public final static int TEXT_ALIGN_LEFT = 0;
    public final static int TEXT_ALIGN_RIGHT = 1;
    public final static int TEXT_ALIGN_CENTER = 2;
    public final static int TEXT_ALIGN_JUSTIFY = 3;

    public abstract int value();

    public static TextAlign create(int value) {
        return new AutoValue_TextAlign(value);
    }

}
