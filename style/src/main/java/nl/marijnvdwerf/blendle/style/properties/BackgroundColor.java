package nl.marijnvdwerf.blendle.style.properties;

import com.google.auto.value.AutoValue;

import nl.marijnvdwerf.blendle.style.StyleDeclaration;
import nl.marijnvdwerf.blendle.style.value.ColorValue;

@AutoValue
public abstract class BackgroundColor extends StyleDeclaration {

    public abstract ColorValue color();

    public static BackgroundColor create(ColorValue color) {
        return new AutoValue_BackgroundColor(color);
    }
}
