package nl.marijnvdwerf.blendle.style;

import com.steadystate.css.parser.SACParserCSS3;

import org.slf4j.Logger;
import org.slf4j.helpers.NOPLogger;
import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.LexicalUnit;

import java.io.IOException;
import java.io.StringReader;

import nl.marijnvdwerf.blendle.style.properties.BackgroundColor;
import nl.marijnvdwerf.blendle.style.properties.Color;
import nl.marijnvdwerf.blendle.style.properties.FontFamily;
import nl.marijnvdwerf.blendle.style.properties.FontSize;
import nl.marijnvdwerf.blendle.style.properties.FontStyleDeclaration;
import nl.marijnvdwerf.blendle.style.properties.FontWeight;
import nl.marijnvdwerf.blendle.style.properties.LetterSpacing;
import nl.marijnvdwerf.blendle.style.properties.LineHeight;
import nl.marijnvdwerf.blendle.style.properties.MarginBottom;
import nl.marijnvdwerf.blendle.style.properties.TextAlign;
import nl.marijnvdwerf.blendle.style.properties.TextDecoration;
import nl.marijnvdwerf.blendle.style.properties.TextIndent;
import nl.marijnvdwerf.blendle.style.properties.TextTransform;
import nl.marijnvdwerf.blendle.style.value.ColorValue;

public class StyleParser {

    private final SACParserCSS3 parser;
    private Logger logger = NOPLogger.NOP_LOGGER;

    public StyleParser() {
        parser = new SACParserCSS3();
    }

    public void setLogger(Logger logger) {
        if (logger != null) {
            this.logger = logger;
        } else {
            this.logger = NOPLogger.NOP_LOGGER;
        }
    }

    public StyleDeclaration[] parseDeclaration(String property, String value) {
        LexicalUnit lexicalUnit = parseValue(value);

        switch (property.toLowerCase()) {
            case "border-top-width":
            case "border-bottom-width":
            case "border-left-width":

            case "border-top-color":
            case "border-bottom-color":
            case "border-left-color":

            case "border-top-style":
            case "border-bottom-style":
            case "border-left-style":

            case "word-spacing":

            case "margin-top":
            case "padding-top":
            case "padding-bottom":
            case "padding-left":

            case "display":
                break;


            case "background-color":
                return array(parseBackgroundColor(lexicalUnit));


            case "color":
                return array(parseColor(lexicalUnit));


            case "text-align":
                return array(parseTextAlign(lexicalUnit));

            case "text-decoration":
                return array(parseTextDecoration(lexicalUnit));

            case "text-indent":
                return array(parseTextIndent(lexicalUnit));

            case "text-transform":
                return array(parseTextTransform(lexicalUnit));

            case "line-height":
                return array(parseLineHeight(lexicalUnit));

            case "letter-spacing":
                return array(parseLetterSpacing(lexicalUnit));


            case "font-family":
                return array(parseFontFamily(lexicalUnit));

            case "font-style":
                return array(parseFontStyle(lexicalUnit));

            case "font-weight":
                return array(parseFontWeight(lexicalUnit));

            case "font-size":
                return array(parseFontSize(lexicalUnit));


            case "margin-bottom":
                return array(parseMarginBottom(lexicalUnit));
        }

        logger.error("Unhandled declaration '%s: %s' <%s:%d>", property, value, lexicalUnit, lexicalUnit.getLexicalUnitType());
        return new StyleDeclaration[]{};
    }

    private StyleDeclaration[] array(StyleDeclaration styleDeclaration) {
        if (styleDeclaration != null) {
            return new StyleDeclaration[]{styleDeclaration};
        }

        return new StyleDeclaration[]{};
    }

    private LexicalUnit parseValue(String value) {
        try {
            return parser.parsePropertyValue(new InputSource(new StringReader(value)));
        } catch (IOException e) {
            logger.error("Error parsing value", e);
        }

        return null;
    }

    private ColorValue parseColorValue(LexicalUnit value) {
        if (value == null) {
            return null;
        }

        if (value.getLexicalUnitType() != LexicalUnit.SAC_RGBCOLOR) {
            logger.error("Only supports rgb colours");
            return null;
        }

        String functionName = value.getFunctionName();
        LexicalUnit parameters = value.getParameters();

        if (functionName.equalsIgnoreCase("rgb")) {
            int red = parameters.getIntegerValue();
            int green = parameters.getNextLexicalUnit().getNextLexicalUnit().getIntegerValue();
            int blue = parameters.getNextLexicalUnit().getNextLexicalUnit().getNextLexicalUnit().getNextLexicalUnit().getIntegerValue();

            return ColorValue.rgb(red, green, blue);
        }

        logger.error("Other functions are not supported yet");
        return null;
    }

    //region Background

    private StyleDeclaration parseBackgroundColor(LexicalUnit value) {
        ColorValue color = parseColorValue(value);

        if (color != null) {
            return BackgroundColor.create(color);
        }

        return null;
    }

    //endregion

    //region Color

    private StyleDeclaration parseColor(LexicalUnit value) {
        ColorValue color = parseColorValue(value);

        if (color != null) {
            return Color.create(color);
        }

        return null;
    }

    //endregion

    //region Text

    private StyleDeclaration parseTextAlign(LexicalUnit value) {
        if (value.getLexicalUnitType() != LexicalUnit.SAC_IDENT) {
            return null;
        }

        if (value.getNextLexicalUnit() != null) {
            return null;
        }


        switch (value.getStringValue().toLowerCase()) {
            case "left":
                return TextAlign.create(TextAlign.TEXT_ALIGN_LEFT);

            case "right":
                return TextAlign.create(TextAlign.TEXT_ALIGN_RIGHT);

            case "center":
                return TextAlign.create(TextAlign.TEXT_ALIGN_CENTER);

            case "justify":
                return TextAlign.create(TextAlign.TEXT_ALIGN_JUSTIFY);
        }

        logger.error("Unsupported value for text-align: %s", value);
        return null;
    }

    private StyleDeclaration parseTextDecoration(LexicalUnit value) {
        if (value.getStringValue().equals("underline")) {
            return TextDecoration.create(TextDecoration.TEXT_DECORATION_UNDERLINE);
        }

        logger.error("Unsupported font-style: %s", value);
        return null;
    }

    private StyleDeclaration parseTextIndent(LexicalUnit value) {
        if (value.getLexicalUnitType() == LexicalUnit.SAC_PIXEL) {
            return new TextIndent(value.getFloatValue());
        }

        logger.error("Only supports pixel indents");
        return null;
    }

    private StyleDeclaration parseTextTransform(LexicalUnit value) {
        if (value.getStringValue().equalsIgnoreCase("uppercase")) {
            return new TextTransform(TextTransform.TRANSFORM_UPPERCASE);
        }

        logger.error("Only supports uppercase");
        return null;
    }

    private StyleDeclaration parseLineHeight(LexicalUnit value) {
        if (value.getLexicalUnitType() == LexicalUnit.SAC_PIXEL) {
            return new LineHeight(value.getFloatValue());
        }

        logger.error("Only supports pixel line heights");
        return null;
    }

    private StyleDeclaration parseLetterSpacing(LexicalUnit value) {
        if (value.getLexicalUnitType() == LexicalUnit.SAC_PIXEL) {
            return LetterSpacing.create(value.getFloatValue());
        }

        return null;
    }

    //endregion

    //region Font

    private StyleDeclaration parseFontFamily(LexicalUnit value) {
        String localFontName = "";
        while (true) {
            localFontName += value.toString();
            if (value.getNextLexicalUnit() != null) {
                localFontName += " ";
                value = value.getNextLexicalUnit();
            } else {
                break;
            }
        }

        return new FontFamily(localFontName);
    }

    private StyleDeclaration parseFontStyle(LexicalUnit value) {
        if (value.getStringValue().equals("italic")) {
            return new FontStyleDeclaration(FontStyleDeclaration.FONT_STYLE_ITALIC);
        }

        if (value.getStringValue().equals("normal")) {
            return new FontStyleDeclaration(FontStyleDeclaration.FONT_STYLE_NORMAL);
        }

        logger.error("Unsupported font-style: %s", value);

        return null;
    }

    private StyleDeclaration parseFontWeight(LexicalUnit value) {
        if (value.getLexicalUnitType() == LexicalUnit.SAC_INTEGER) {
            return new FontWeight(value.getIntegerValue());
        }

        logger.error("Only supports integer font weights");

        return null;
    }

    private StyleDeclaration parseFontSize(LexicalUnit value) {
        if (value.getLexicalUnitType() == LexicalUnit.SAC_PIXEL) {
            return new FontSize(value.getFloatValue());
        }

        logger.error("Only supports pixel font sizes");
        return null;
    }

    //endregion


    //region Margin & Padding

    private StyleDeclaration parseMarginBottom(LexicalUnit value) {
        if (value.getLexicalUnitType() == LexicalUnit.SAC_PIXEL) {
            return new MarginBottom(value.getFloatValue());
        }

        logger.error("Only supports pixel margins");
        return null;
    }

    //endregion
}
