package nl.marijnvdwerf.blendle.style.properties;

import nl.marijnvdwerf.blendle.style.StyleDeclaration;

public class TextIndent extends StyleDeclaration {
    public final float length;

    public TextIndent(float indent) {
        this.length = indent;
    }
}
