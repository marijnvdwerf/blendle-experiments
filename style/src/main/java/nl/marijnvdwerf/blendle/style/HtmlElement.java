package nl.marijnvdwerf.blendle.style;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class HtmlElement {

    public abstract String tagName();

    public abstract String id();

    public abstract String className();

    public abstract HtmlElement withId(String id);

    public abstract HtmlElement withClassName(String className);

    public static HtmlElement create(String tagName) {
        return new AutoValue_HtmlElement(tagName, "", "");
    }

    public String[] classList() {
        return className().split("\\s+");
    }
}
