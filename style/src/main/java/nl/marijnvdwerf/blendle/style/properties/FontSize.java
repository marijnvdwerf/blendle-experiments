package nl.marijnvdwerf.blendle.style.properties;

import nl.marijnvdwerf.blendle.style.StyleDeclaration;

public class FontSize extends StyleDeclaration {
    public final float size;

    public FontSize(float size) {
        this.size = size;
    }
}
